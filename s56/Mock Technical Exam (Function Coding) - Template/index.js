function countLetter(letter, sentence) {
    let result = 0;

    if (letter.length == 1){
        let arrSentence = sentence.split("");
        for(let i = 0; i< arrSentence.length ; i++){
            if(letter == arrSentence[i]){
                result += 1;
            }
        }

        return result
    } else {
        return undefined
    }

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
    let newText = text.toLowerCase();
    let arrNewText = newText.split("");

    const duplicates = arrNewText.filter((item, index) => index !== arrNewText.indexOf(item));

    if (duplicates.length !== 0){
        return false
    } else {
        return true
    }



    
}

function purchase(age, price) {
    let result = 0
    if(age<13){
        /*result = undefined;
        return result.toString();*/
        return undefined
    } else if((age >= 13 && age <= 21) || age >= 65 ){
        result = (price-(price*0.2));
        return result.toFixed(2);
        //return (price-(price*0.2));
    } else if(age >= 22 && age <= 64 ){
        result = (price);
        return result.toFixed(2);
        //return Math.round(price);
    }
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

function findHotCategories(items) {


    let hot = [];

    items.forEach(function(item){
        if(item.stocks == 0){

            if(hot.includes(item.category)){

            } else {
                hot.push(item.category)
            }
            
        }
    })

    return hot;
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.


    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    const voters = candidateA.filter(value => candidateB.includes(value));

    return voters;
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};