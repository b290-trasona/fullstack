//console.log("asd");

// fetch() method in Javascript is used to send request in the server and load the received response in the webpage/s. The request and response is in JSON format.

/*
	syntax
		fetch("url",(options))
		url - this is the address which the request is to be made and source where the response will come from (endpoint)
		options - array or properties that contains the HTTP method, body of request and headers.
*/

//https://jsonplaceholder.typicode.com/posts
//get post data
fetch("https://jsonplaceholder.typicode.com/posts")
	.then(res => res.json())
	.then(data => showPosts(data));


//view post == to display each post from jsonplaceholder

const showPosts = posts => {

	//Create a variable that will contain all the posts
	let postEntries = ""

	posts.forEach(post => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>


		`
	});

	console.log(postEntries);

	document.querySelector("#div-post-entries").innerHTML = postEntries
};

//const showTwoPosts = posts =>

fetch("https://jsonplaceholder.typicode.com/posts/20")
	.then(res => res.json())
	.then(data => console.log(data));

fetch("https://jsonplaceholder.typicode.com/posts/21")
	.then(res => res.json())
	.then(data => console.log(data));

document.querySelector("#form-add-post").addEventListener("submit", e => {

	//prevents the page from reloading.
	e.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts",
	{
		method : "POST",
		body: JSON.stringify({
			tile: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 290
		}),
		headers: {
			"Content-Type": "application/json"
		}

	}).then(res => res.json())
	.then(data=> {
		console.log(data);
		alert("asda");
	})

	document.querySelector("#txt-title").value = null;
	document.querySelector("#txt-body").value = null;
})

//Edit post data

const editPost = id => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector(`#txt-edit-id`).value = id;
	document.querySelector(`#txt-edit-title`).value = title;
	document.querySelector(`#txt-edit-body`).value = body;

	document.querySelector(`#button-submit-update`).removeAttribute("disabled");




};

//update post data
document.querySelector("#form-edit-post").addEventListener("submit", e => {

	e.preventDefault();

	let id = document.querySelector("#txt-edit-id").value;

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,{
		method: "PUT",
		body: JSON.stringify({
			id: id,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 290
		}),
		headers : {
			"Content-Type": "application/json"
		}

	}).then(res=> res.json())
	.then(data => {
		console.log(data);
		alert("successfully updated")
	});

	// Reset input fields once submitted
	document.querySelector("#txt-edit-title").value = null;
	document.querySelector("#txt-edit-body").value = null;
	// Resetting disabled attribute for button
	document.querySelector("#button-submit-update").setAttribute("disabled",true)
})

const deletePost = id =>{

	fetch(`https://jsonplaceholder.typicode.com/posts/${id}`,
			{
				method : "DELETE",
				/*body : JSON.stringify({
					id : id,
					// the value will be from the value of input fields or HTML elements
					title : document.querySelector("#txt-edit-title").value,
					body : document.querySelector("#txt-edit-body").value,
					userId : 290
				}),*/
				headers : {
					"Content-Type" : "application/json"
				}
			}).then(res => res.json())
				.then(data => {
					console.log(data);
					alert("Post successfully Deleted!")
				});



	document.querySelector(`#post-${id}`).remove()
}





