import { Fragment } from 'react';
import {useState,useEffect} from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';


import AppNavbar from './components/AppNavbar';
  // already imported to Home.js
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Error from './pages/Error';

import './App.css';

import { UserProvider } from './UserContext'

function App() {

  // State hook for the user state that's defined here for a global scope
        // Initialized as an object with properties from the localStorage
        // This will be used to store the user information and will be used for validating if a user is logged in on the app or not

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  // Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    

    fetch("http://localhost:4000/users/details", {
      headers: {
        Authorization : `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data._id)

      // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
      console.log(typeof data._id)
      if(data._id){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
    })

    console.log(user.id)
    console.log(user)
    console.log(user.isAdmin)

  },[])

  return (
 
    // <Fragment>      
    //   <AppNavbar />
    //   <Banner />
    // </Fragment>

    // - Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop
    
    <UserProvider value={{user,setUser,unsetUser}}>
      <Router>
        <Container fluid>
            <AppNavbar />
            <Container>
              <Routes>
                  <Route path="/" element={<Home />} />
                  <Route path="/courses" element={<Courses />} />
                  <Route path="/courses/:courseId" element={<CourseView />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/login" element={<Login />} />
                  <Route path="/logout" element={<Logout />} />
                  <Route path="*" element={<Error />} />
              </Routes>
            </Container>          
        </Container>
      </Router>
    </UserProvider>

      
  );
}

export default App;

/*
  - JSX syntax requires that components should always have closing tags.
  - In the example above, since the "AppNavbar" component does not require any text to be placed in between, we add a "/" at the end to signify that they are self-closing tags. 
  - It's also good practice to organize importing of modules to improve code readability.
  - The example above follows the following pattern:
    - imports from built-in react modules
    - imports from downloaded packages
    - imports from user defined components
*/
