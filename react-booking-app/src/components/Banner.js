import {Button,Col,Row} from 'react-bootstrap';
import {Link} from 'react-router-dom'

export default function Banner(props) {
	console.log(props.page)
	return (
		(props.page == "home") ?
			<Row>
				<Col className = "p-5">
					<h1>Zuitt</h1>
					<p>Opportunities for everyone, everywhere</p>
					<Button variant="primary">Enroll Now</Button>
				</Col>
			</Row>

			:

			<Row>
				<Col className = "p-5">
					<h1>Page Not Found</h1>
					<p>Go back to <Link to="/">homepage</Link></p>
				
					
				</Col>
			</Row>

	)
}

/*- The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.*/