import {useEffect,useState} from 'react';
import CourseCard from '../components/CourseCard'

//import coursesData from '../data/coursesData';

export default function Courses(){
	/*console.log(coursesData);
	console.log(coursesData[0]);*/

	const [course, setCourse] = useState([]);

	// Retrieves the courses from the database upon initial render of the "Courses" component
	useEffect(()=> {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setCourse(data.map(course =>{
					return(
						<CourseCard key={course._id} course ={course} />
					)
				}))
			})
	},[]);

	// const courses =coursesData.map(course =>{
	// 	return(
	// 		<CourseCard key={course.id} course ={course} />
	// 	)
	// })
	return(

		<>
			{/*<CourseCard course={coursesData[0]}/>*/}
			{course}
		</>


	)
}