let collection = [];

// Write the queue functions below.

function print() {
	return collection;
}

function enqueue(element) {
	let length = 0;
	for(let i=0;true;i++){
		if(collection[i]== undefined){
			break;
		} else {
			length++;
		}
	}

	collection[length] = element;
	return collection;

	// collection.push(element);
	// return collection;
}

function dequeue() {
	let length = 0;
	for(let i=0;true;i++){
		if(collection[i] == undefined){
			break;
		} else {
			length++;
		}
	}

	let newArr =[];
	//let x = 0;
	for(let j=1;j < length; j++){
		newArr[j-1] = collection[j];
	}

	collection = newArr;

	return collection;

	//collection.shift();
	//return collection;
}

function front() {
	return collection[0];
}

function size() {
	let length = 0;
	for(let i=0;true;i++){
		if(collection[i]== undefined){
			break;
		} else {
			length++;
		}
	}

	return length;
	//return collection.length
}

function isEmpty() {
	let length = 0;
	for(let i=0;true;i++){
		if(collection[i]== undefined){
			break;
		} else {
			length++;
		}
	}
//Changed from 'if(length > 1)' to if(length >= 1) after submission  
	if(length>=1){
		return false
	} else {
		return true
	}
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};